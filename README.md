Chiru Service Fusion AR - Sensor App
==========

This project is a part of the Chiru Service Fusion project, providing tablet sensor reading.

#Development

Before developing on this project, please read the following carefully:

##Install eclipse for Android

Google now publishes ADT (Android Development Toolkit) bundled nicelly with Eclipse. 

###Windows

* Download JRE from [Oracle download site](http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html) and install it.
* Download ADT bundle from [Google Android site](http://developer.android.com/sdk/index.html). Unpack and start Eclipse. 

###Ubuntu


###Once in Eclipse

* When eclipse is starting, it will ask for workspace. Select any location you want. It should be a new folder. You should not actually use this folder tostore any files in it yourself. Dismiss welcome page.
* Select menu Window -> Android SDK Manager. Download 4.2, 4.0 and 2.3 and at least following of each:
  * SDK Platform
  * Intel system image
  * Gogle APIs.
* Also install all tools, and Google USB driver from Extras.
* If you want to use the phone, you will need to install its drivers (on Windows)
  * [Samsung Galaxy S2](http://www.samsung.com/uk/support/model/GT-I9100LKAXEU-downloads?isManualDownload=true#) (and probably all other Samsung phones). Select software, and then the one that says (quite descriptively): "Uninstall program,USB Driver". Download and install that.
* Fetch the stuff from the repos. Make sure to use correct branch!
* Import the project into workspace:
  * Manu File -> Import
  * Android (opens) -> Existing Android Code into Workspace -> Next
  * Root Directory - browse to the root of the project.
  * **Make sure "Copy projects into workspace" is not checked.**
  * Finish.
* Create debug configuration 
  * Menu: Run -> Debug configurations.
  * Select Android Application and then click the Add icon (left most above the list)
  * Name the configuraion, select project, click Apply
  * Select the target tab, and I suggest you select "Always prompt to pic kdevice". At least until you are sure your emulator works. Click apply.
* Debug keys - since we are likely to share devices, there will be a problem installing latest built SW onto devices, as Android will reject updating if the signatures do not match. Android signs applications by default using its own keys generated first time it is used. So we should all use same debug keys (which will anyhow not be allowed unless signed by Google).
  * Move the files from the project root folder / adb_keys into ~/.android directory.
  * If you do not want to do this, simply uninstall previous application from the phone before putting new version.


**Note about the project refreshing!**

> Eclipse has problems knowing about code changes if not done from Eclipse - even if you restart Eclipse, it might not be enough. You will be receiving weird errors because of this.
> 
> Simple solution, which should be employed every time any error occurs, is to right-click the project in the Package Explorer (on the left) and select Refresh (or press F5).

**Note about applications signatures!**

###Debugging/testing using emulator

I am not doing this. It takes so long time. You should only do this if you do not have a device.

###Debugging/testing on a device

Tested with Galaxy S II and RAZRi

* In the phone: Applications -> Settings -> Applications -> Development -> check Use USB debugging.
* Also in the phone: Applications -> Settings -> Applications -> check Unknown sources. (See above about adb keys.)
* Connect the phone, make sure drivers install.
* In eclipse, click the debug icon (the little green bug or menu Run -> Debug).
* Config dialog pops up. There should be phone shown in the "Choose a running Android device". If not, check if the drivers are installed correctly, and if the phone is connected correctly. Now is good time to select "Always use...".
* Click OK!
* Have fun!

##Phone settings

###GPS

Android has location system as a separate subsystem - and locoation means many things, not only GPS. Our system is tested using only the GPS location information. To set it that way:

* Open Applications -> Settings -> Location and services (older versions security)
* Enable Use GPS satellites
* Disable everything else (Use wireless networks aka Google's location services, Use sensor aiding or Location & Google search).

This has been tested on Galaxy S2 and RAZRi, might be different on other phones versions.

